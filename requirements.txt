mkdocs==1.5.3
mkdocs-material==9.4.2
mkdocs-awesome-pages-plugin==2.9.2
mkdocs-git-revision-date-localized-plugin==1.2.0
mkdocs-rss-plugin==1.8.0
md-tooltips==1.3.1
