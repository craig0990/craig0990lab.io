---
hide:
  - navigation
  - toc
---

# Projects

## [Dev](https://github.com/craig0990/dev/)

> A simple Bash task runner for running project or directory specific tasks in
> a `Devfile`

## [mkdocs-plugin-inline-svg](https://gitlab.com/craig0990/mkdocs-plugin-inline-svg)

> Reads SVG images referenced from Markdown and replaces them with the SVG file
> content
